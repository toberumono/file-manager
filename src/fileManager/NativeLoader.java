package fileManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;

/**
 * Manages Native Libraries. This is a <i>static</i> class.
 * 
 * @author Toberumono
 */
public class NativeLoader {
	private static TemporaryFileManager manager;
	
	static {
		try {
			String jarName = new File(NativeLoader.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getAbsolutePath();
			manager = new TemporaryFileManager(jarName.substring(jarName.lastIndexOf(System.getProperty("file.separator")) + 1, jarName.lastIndexOf('.')), true);
		}
		catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			try {
				manager = new TemporaryFileManager();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static String os = "", extension = "";
	
	//This shouldn't be initialized
	private NativeLoader() {}
	
	/**
	 * Attempts to load the named libraries. It automatically determines the proper extension for the library to load, and
	 * then unpacks it from the .jar and loads it.<br>
	 * All the files that this library unpacks are considered temporary and are deleted after each run, if possible.
	 * 
	 * @param libs
	 *            the names (without extensions) of the libraries to load
	 * @return the names of the libraries that failed to load (so, if this list is empty, the entire load was successful)
	 */
	public static synchronized String[] loadNatives(String... libs) {
		if (libs.length == 0)
			return libs;
		String extension = getExtension();
		String[] errors = new String[libs.length];
		int error = 0;
		for (int i = 0; i < libs.length; i++) {
			InputStream io = NativeLoader.class.getResourceAsStream("/native libraries/" + libs[i] + extension); //Check if the file is in the .jar.  There probably is a better way, though.
			if (io == null) {
				//If the file isn't in the .jar, try adding lib to the front and see if that works (compatibility with System.loadLibrary())
				io = NativeLoader.class.getResourceAsStream("/native libraries/lib" + libs[i] + extension);
				if (io == null)
					errors[error++] = libs[i];
				else
					System.load(manager.loadFiles("/native libraries/lib" + libs[i] + extension)[0].toString()); //Load the library with the added lib prefix
			}
			else
				System.load(manager.loadFiles("/native libraries/" + libs[i] + extension)[0].toString()); //Load the library
		}
		//Generate the errors list
		String[] output = new String[error];
		for (int i = 0; i < output.length; i++)
			output[i] = errors[i];
		return output;
	}
	
	/**
	 * @return the {@link Path} being used to store the native libraries
	 */
	public static synchronized Path getPath() {
		return manager.getPath();
	}
	
	/**
	 * @return the detected operating system
	 */
	public static synchronized String getOS() {
		if (os.length() > 0)
			return os;
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.indexOf("win") > -1)
			os = "windows";
		else if (OS.indexOf("mac") > -1)
			os = "mac";
		else
			//Because if the os cannot be identified, it is probably a version of linux/unix.
			os = "linux";
		return os;
	}
	
	/**
	 * @return the calculated native library extension
	 */
	public static synchronized String getExtension() {
		if (extension.length() > 0)
			return extension;
		switch (getOS()) {
			case "windows":
				extension = ".dll";
				break;
			case "mac":
				extension = ".jnilib";
				break;
			case "linux":
				extension = ".so";
				break;
			default:
				extension = ".so";
		}
		return extension;
	}
}
