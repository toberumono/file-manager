package fileManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import toberumono.structures.collections.lists.SortedList;

/**
 * Root class for this library. It supports threading.
 * 
 * @author Toberumono
 */
public class FileManager {
	protected final Path path;
	protected final FileSystem fs;
	protected final boolean existed;
	protected final List<Path> loaded, paths;
	protected final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	
	/**
	 * Initializes using <tt>directory</tt> as the path to the directory to use. If the directory does not exist, it attempts
	 * to create it.
	 * 
	 * @param directory
	 *            the path to the directory to use.
	 * @throws IOException
	 *             if there is an error in the file handling operations
	 */
	public FileManager(String directory) throws IOException {
		this(FileSystems.getDefault().getPath(directory));
	}
	
	/**
	 * Initializes using <tt>directory</tt> as the absolute path to the directory to use. If the directory does not exist, it
	 * attempts to create it.
	 * 
	 * @param directory
	 *            the {@link java.nio.file.Path Path} denoting the absolute path to the directory to use
	 * @throws IOException
	 *             if there is an error in the file handling operations
	 */
	public FileManager(Path directory) throws IOException {
		lock.writeLock().lock();
		fs = directory.getFileSystem();
		this.path = directory;
		if (!(existed = Files.exists(path)))
			Files.createDirectories(directory);
		lock.writeLock().unlock();
		loaded = new SortedList<>(Path::compareTo);
		paths = new SortedList<>(Path::compareTo);
		paths.add(path);
	}
	
	/**
	 * Loads one or more files from within the jar
	 * 
	 * @param paths
	 *            the paths to the desired files relative to the root of the jar
	 * @return the loaded files with null inserted for files that failed to load
	 */
	public synchronized File[] loadFiles(String... paths) {
		if (paths.length == 0)
			return new File[0];
		File[] output = new File[paths.length];
		for (int i = 0; i < paths.length; i++) {
			if (loaded.contains(paths[i])) { //If it has already been unpacked, just return the appropriate File
				output[i] = new File(path + paths[i]);
				continue;
			}
			InputStream io = FileManager.class.getResourceAsStream(paths[i]); //Otherwise, get the io stream from the .jar
			if (io == null) { //If the stream is null, then there was an eror, return null for this file
				output[i] = null;
				continue;
			}
			try {
				Path p = path.getFileSystem().getPath(path.toString(), paths[i]).toAbsolutePath(), temp = p;
				do {
					this.paths.add(temp);
				} while ((temp = temp.getParent()) != null && !this.paths.contains(temp.toString()));
				setFile(io, p); //Unpack the file
				output[i] = p.toFile(); //return the appropriate File
				loaded.add(Paths.get(paths[i])); //save the information on it
			}
			catch (IOException e) {
				e.printStackTrace(); //If there was an IO exception, print it, and return null for this file
				output[i] = null;
			}
		}
		return output;
	}
	
	/**
	 * Unpacks the file designated by the <tt>InputStream</tt> to the location designated by fileName, and creates the
	 * necessary directories if they do not already exist.
	 * 
	 * @param io
	 *            the <tt>InputStream</tt> pointing to the file that is to be unpacked
	 * @param fileName
	 *            the path with the file name to which the file should be unpacked
	 * @throws IOException
	 *             if there is an error in the file handling operations
	 */
	protected synchronized void setFile(InputStream io, Path fileName) throws IOException {
		if (fileName.getParent() != null) {
			File check = fileName.getParent().toFile();
			if (!check.exists())
				check.mkdirs();
		}
		FileOutputStream fos = new FileOutputStream(fileName.toString());
		byte[] buf = new byte[256];
		int read = 0;
		while ((read = io.read(buf)) > 0)
			fos.write(buf, 0, read);
		fos.close();
	}
	
	/**
	 * @return the <tt>Path</tt> to the directory to which this <tt>FileManager</tt> extracts files.
	 */
	public Path getPath() {
		return path;
	}
	
	/**
	 * @return the name of the .jar file from which this <tt>FileManager</tt> was loaded
	 */
	protected static final String getJarName() {
		try {
			String jarName = new File(FileManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getAbsolutePath();
			return jarName.substring(jarName.lastIndexOf(System.getProperty("file.separator")) + 1, jarName.lastIndexOf('.'));
		}
		catch (URISyntaxException e) {
			return "";
		}
	}
}
