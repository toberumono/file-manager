package fileManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * An implementation of {@link FileManager} specifically for temporary files.
 * 
 * @author Toberumono
 */
public class TemporaryFileManager extends FileManager {
	
	/**
	 * Initializes using a folder in the system default temporary directory
	 * 
	 * @throws IOException
	 *             if the temporary folder could not be initialized
	 */
	public TemporaryFileManager() throws IOException {
		this(getJarName(), true);
	}
	
	/**
	 * Initializes using the specified directory, either relative to the filesystem root or the default temporary directory
	 * depending on the value of useTemporaryDirectory
	 * 
	 * @param directory
	 *            the directory to initialize
	 * @param useTemporaryDirectory
	 *            whether this should be in a temporary directory
	 * @throws IOException
	 *             if the temporary folder could not be initialized
	 */
	public TemporaryFileManager(String directory, final boolean useTemporaryDirectory) throws IOException {
		super(useTemporaryDirectory ? Files.createTempDirectory(directory).toString() : directory);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				paths.remove(path.toString());
				while (paths.size() > 0) {
					File file = paths.remove(0).toFile();
					file.delete();
				}
				if (useTemporaryDirectory && !existed)
					new File(path.toString()).delete();
			}
		});
	}
}
